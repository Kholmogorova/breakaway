CC=gfortran

CFLAGS=-c -Wall -O0 -g

all: breakaway.out

breakaway.out: breakaway.o functions_ba.o moduls_ba.o full_speed.o
	$(CC) $^ -o $@ -llapack

breakaway.o: breakaway.f90 moduls_ba.o
	$(CC) $(CFLAGS) $< 

functions_ba.o: functions_ba.f90
	$(CC) $(CFLAGS) $<

moduls_ba.o: moduls_ba.f90 
	$(CC) $(CFLAGS) $< 

full_speed.o: full_speed.f90
	$(CC) $(CFLAGS) $< 

discrete_circle.out: discrete_circle.f90
	$(CC) -Wall -O2  $< -o $@

clean:
	rm -rf *.o
graph:
	cat result.graph | gnuplot
