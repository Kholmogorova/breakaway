program discrete_circle
implicit none
    integer :: point_num, i, input_id, output_id
    real(8) :: angle, angle_difference
    character(len = 20) :: output
    real(8) :: x, y


    input_id = 1
    open (input_id, file = "input_dc")
    read (input_id, *) point_num
    read (input_id, *) output
    close(input_id)

    output_id = input_id + 1
    open (output_id, file = output)

    angle = 0
    angle_difference = 2 * acos(-1.d0) / point_num

    write(output_id, *) point_num

    do i = 1, point_num
        angle = angle + angle_difference
        x = cos(angle)
        y = sin(angle)
        write(output_id, *) x, y
    end do

    close(output_id)
end program
