program breakaway 
!use step
implicit none
include "interface.f90"

!main

    integer ::                                                      & 
                point_num,                                          &
                step_num,                                           &
                breakaway_num,                                      &
                cur_track_num,                                      &
                track_num,                                          &
                input_id,                                           &
                data_id,                                            &
                parameter_id,                                       &
                geom_id,                                            &
                sheet_id,                                           &
                id_counter,                                         &
                i, info
    integer, allocatable ::                                         &
                            pivot_indicies(:),                      &
                            breakaways(:)
    character(len = 20) ::                                          &
                            data_file,                              &
                            parameter_file,                         &
                            geom_file,                              &
                            sheet_file
    real(8) ::                                                      &
                regular_speed(2),                                   &
                rotation_radius,                                    &
                rotation_radius_sq,                                    &
                time_interval
    real(8), allocatable ::                                         &
                            rotation_points(:, :),                  & 
                            control_points(:, :),                   &
                            normals(:, :),                          &
                            gyration(:),                            &
                            system(:, :),                           &
                            track(:, :),                            &
                            track_speed(:, :),                      &
                            track_circle_speed(:)

    regular_speed(1) = 1.d0
    regular_speed(2) = 0.d0

    input_id = 1
    id_counter = 1
    open (input_id, file = "input_ba")
    read (input_id, *) data_file
    read (input_id, *) parameter_file
    read (input_id, *) geom_file
    read (input_id, *) sheet_file
    close(input_id)

    data_id = id_counter
    parameter_id = id_counter + 1
    geom_id = id_counter + 2
    id_counter = id_counter + 3

    open (data_id, file = data_file)
    read (data_id, *) point_num
    open (parameter_id, file = parameter_file)
    
    call read_parameters(parameter_id, rotation_radius,             & 
            time_interval, step_num, breakaway_num)
    track_num = breakaway_num * (step_num + 1)
    
    allocate (rotation_points(2, point_num))
    allocate(control_points(2, point_num))
    allocate(normals(2, point_num))
    allocate(gyration(point_num + 1))
    allocate(system(point_num + 1, point_num + 1))
    allocate(pivot_indicies(point_num + 1))
    allocate(breakaways(breakaway_num))
    allocate(track(2, track_num))
    allocate(track_speed(2, track_num))
    allocate(track_circle_speed(track_num))
    
    call read_points(rotation_points, point_num, data_id)
    close(data_id)

    read(parameter_id, *) (breakaways(i), i = 1, breakaway_num)
    close(parameter_id)
    
!    call get_step_size(rotation_points)
    rotation_radius_sq = rotation_radius * rotation_radius
    call get_control_points(control_points,                         & 
                            rotation_points, point_num)
    call get_normals(normals, rotation_points, point_num)
    
!    open(geom_id, file = geom_file)
!    call output_geometry(geom_id, 1, 1, 0, point_num,               &
!                    rotation_points, breakaway_num, breakaways)
!    close(geom_id)

    call get_system(system, control_points,                         & 
            rotation_points, normals, point_num, rotation_radius_sq)

    call dgetrf(point_num + 1, point_num + 1, system,               &
                point_num + 1, pivot_indicies, info)
    if (info .ne. 0) then
        stop
    end if
    call solution(point_num, 0, gyration, normals, regular_speed,   &
            control_points, track, track_circle_speed,              &
            pivot_indicies, system, rotation_radius_sq)

    do i = 1, breakaway_num
        track(:, i) = rotation_points(:, breakaways(i))
    end do
    cur_track_num = breakaway_num

    sheet_id = id_counter
    id_counter = id_counter + 1
    open(sheet_id, file = sheet_file, form = 'unformatted',         &
            access = 'stream')

! basic cycle
    do i = 1, step_num
        call track_shift(point_num, track_num, track, rotation_points,  &
               track_circle_speed, gyration, regular_speed,             &
               track_speed, breakaway_num, i, breakaways,              &    
               time_interval, rotation_radius, rotation_radius_sq)
        call solution(point_num, cur_track_num, gyration, normals,      &
               regular_speed, control_points, track,                    &
               track_circle_speed, pivot_indicies, system,              &
               rotation_radius_sq)
        cur_track_num = cur_track_num + breakaway_num
        call make_sheet(sheet_id, cur_track_num, i, breakaway_num, track) 
!        call output(sheet_id, cur_track_num, i, breakaway_num, track,   &
!                rotation_points, point_num, 1)
    end do
    
    close(sheet_id)

    deallocate(rotation_points)
    deallocate(control_points)
    deallocate(normals)
    deallocate(gyration)
    deallocate(system)
    deallocate(pivot_indicies)
    deallocate(breakaways)
    deallocate(track)
    deallocate(track_speed)
    deallocate(track_circle_speed)
    
end program
