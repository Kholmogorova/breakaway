subroutine rotation(target_point, rot_location, res, rotation_radius_sq)
implicit none
    real(8) :: target_point(2), rot_location(2), res(2)
    real(8) :: divisor, aux_vect(2)
    real(8) :: pi = 3.141592653589793, rotation_radius_sq

    aux_vect = target_point - rot_location
    divisor = dot_product(aux_vect, aux_vect)
    if (divisor .le. rotation_radius_sq) then
        divisor = rotation_radius_sq
    end if
    divisor = pi * 2 * divisor
    res(1) = -aux_vect(2) / divisor
    res(2) = aux_vect(1) / divisor
end subroutine

subroutine get_right_part(right_part, normals, regular_speed,           &
            control_points, track, track_circle_speed,                  &
            track_num, point_num, rotation_radius_sq)
implicit none
    integer :: point_num, track_num
    real(8) :: right_part(point_num + 1)
    real(8) :: normals(2, point_num), regular_speed(2)
    real(8) :: control_points(2, point_num), track(2, track_num)
    real(8) :: track_circle_speed(track_num)

    real(8) :: aux(2), rotation_radius_sq
    integer :: i
    do i = 1, point_num
        aux = 0
        call full_speed(track, track_circle_speed, track_num,           &
                    control_points(:, i), aux, rotation_radius_sq)
        aux = regular_speed + aux
        right_part(i) = -dot_product(aux, normals(:, i))
    end do
    right_part(point_num + 1) = 0.d0
end subroutine

subroutine get_system(system, control_points, points, normals,          & 
                        point_num, rotation_radius_sq)
implicit none
    integer :: point_num, i, j
    real(8) ::  control_points(2, point_num),                           &
                points(2, point_num),                                   &
                normals(2, point_num),                                  &
                system(point_num + 1, point_num + 1)
    real(8) ::  aux_vect(2), rotation_radius_sq

    do j = 1, point_num
        do i = 1, point_num
            call rotation(control_points(:, i), points(:, j),           &
            aux_vect, rotation_radius_sq)
            system(i, j) = dot_product(aux_vect, normals(:, i));
        end do
        system(point_num + 1, j) = 1.d0
    end do
    do i = 1, point_num
        system(i, point_num + 1) = 1.d0
    end do
    system(point_num + 1, point_num + 1) = 0.d0
end subroutine

subroutine full_speed(points, circle_speed, point_num,                  &
                        place, res, rotation_radius_sq)
implicit none
    integer :: point_num, j
    real(8) :: points(2, point_num), circle_speed(point_num) 
    real(8) :: aux_vect(2), res(2), place(2), rotation_radius_sq

    res = 0
    do j = 1, point_num
        call rotation(place, points(:, j), aux_vect, rotation_radius_sq)
        res = res + circle_speed(j) * aux_vect
    end do
end subroutine

subroutine create_speed(points, circle_speed, point_num,                &
        net_size, net_point, regular_speed, speed, rotation_radius_sq)
implicit none
    integer :: point_num, net_size
    real(8) :: points(2, point_num), circle_speed(point_num),           &
                net_point(2, net_size), regular_speed(2),               &
                speed(2, net_size), rotation_radius_sq
    integer :: i

    do i = 1, net_size
        call full_speed(points, circle_speed, point_num,                &
                        net_point(:, i), speed(:, i), rotation_radius_sq)
        speed(:, i) = speed(:, i) + regular_speed
    end do
end subroutine

subroutine speed_with_track(target_point, res,                          &
            track_num, point_num, track, points,                        &
            track_circle_speed, circle_speed, regular_speed,            &
            rotation_radius_sq)
implicit none
    integer :: point_num, track_num
    real(8) :: target_point(2), res(2), track(2, track_num)
    real(8) :: points(2, point_num), circle_speed(point_num)
    real(8) :: track_circle_speed(track_num), regular_speed(2)
    real(8) :: rotation_radius_sq
    real(8) :: aux(2)

    res = regular_speed
    call full_speed(points, circle_speed, point_num,                    &
                    target_point, aux, rotation_radius_sq)
    res = res + aux
    call full_speed(track, track_circle_speed, track_num,               &
                    target_point, aux, rotation_radius_sq)
    res = res + aux 
end subroutine

subroutine track_shift(point_num, track_num, track, points,             &
            track_circle_speed, circle_speed, regular_speed,            &
            track_speed, breakaway_num, step_num, breakaways,           &
            time_interval, rotation_radius, rotation_radius_sq)
implicit none
    integer :: point_num, track_num, breakaway_num, step_num
    real(8) :: track(2, track_num), points(2, point_num)
    real(8) :: track_circle_speed(track_num), circle_speed(point_num)
    real(8) :: regular_speed(2), track_speed(2, track_num)
    integer :: breakaways(breakaway_num)
    real(8) :: rotation_radius_sq

    integer :: i, cur_track_num
    real(8) :: norm, time_interval, rotation_radius    

    cur_track_num = breakaway_num * step_num
    do i = 1, cur_track_num
        call speed_with_track(track(:, i), track_speed(:, i),           &
            cur_track_num - breakaway_num, point_num, track, points,    &
            track_circle_speed, circle_speed, regular_speed,            &
            rotation_radius_sq)
    end do
    do i = 1, cur_track_num
        track(:, i) = track(:, i) + time_interval * track_speed(:, i)
        norm = dot_product(track(:, i), track(:, i))
        if (norm .le. 1.d0) then
            track(:, i) = track(:, i) *                                 & 
                        (1.d0 + rotation_radius) / sqrt(norm)
        end if
    end do
    do i = 1, breakaway_num
        track(:, cur_track_num + i) = points(:, breakaways(i))
        track_circle_speed(i + cur_track_num - breakaway_num) =         &
                circle_speed(breakaways(i))
    end do
    
end subroutine

subroutine solution(point_num, track_num, gyration, normals,            &
            regular_speed, control_points, track,                       &
            track_circle_speed, pivot_indicies, system,                 &
            rotation_radius_sq)
implicit none
    integer :: point_num, track_num, info,                              &
                pivot_indicies(point_num + 1)
    real(8) ::  gyration(point_num + 1),                                &
                normals(point_num),                                     &
                regular_speed(2),                                       &
                control_points(2, point_num),                           &
                track(2, track_num),                                    &
                track_circle_speed(track_num),                          &
                system(point_num + 1, point_num + 1),                   &
                rotation_radius_sq

    call get_right_part(gyration, normals, regular_speed,               &
                    control_points, track, track_circle_speed,          &
                    track_num, point_num, rotation_radius_sq)
    call dgetrs('N', point_num + 1, 1, system, point_num + 1,           &
            pivot_indicies, gyration, point_num + 1, info)
    if (info .ne. 0) then
        stop
    end if
end subroutine

subroutine get_control_points(control_points, points, point_num)
implicit none
    integer :: point_num, i
    real(8) :: control_points(2, point_num), points(2, point_num)

    do i = 1, point_num - 1
        control_points(:, i) = (points(:, i) + points(:, i + 1)) / 2
    end do
    control_points(:, point_num) = (points(:, point_num) + &
                                    points(:, 1)) / 2
end 

subroutine get_normals(normals, points, point_num)
implicit none
    integer :: point_num, i
    real(8) :: normals(2, point_num), points(2, point_num)
    real(8) :: aux_vect(2), norm

    do i = 1, point_num - 1
        aux_vect = points(:, i + 1) - points(:, i)
        norm = sqrt(dot_product(aux_vect, aux_vect))
        normals(1, i) = aux_vect(2) / norm
        normals(2, i) = -aux_vect(1) / norm
    end do
    aux_vect = points(:, 1) - points(:, point_num)
    norm = sqrt(dot_product(aux_vect, aux_vect))
    normals(1, point_num) = aux_vect(2) / norm
    normals(2, point_num) = -aux_vect(1) / norm
end subroutine
