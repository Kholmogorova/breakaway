    void get_control_points_(double *control_points, double *points,
            int *point_num);

    void get_normals_(double *normals, double *points, int *point_num);

    void get_system_(double *system, double *control_points, 
            double *points, double *normals, int *point_num,
            double *rotation_radius_sq);

    void track_shift_(int *point_num, int *track_num, double *track, 
            double *points, double *track_circle_speed, 
            double *circle_speed, double *regular_speed, 
            double *track_speed, int *breakaway_num, 
            int *step_num, int *breakaways, double *time_interval,
            double *rotation_radius, double *rotation_radius_sq);

    void solution_(int *point_num, int *track_num, double *gyration,
            double *normals, double *regular_speed, 
            double *control_points, double *track,
            double *track_circle_speed, int *pivot_indicies,
            double *system, double *rotation_radius_sq);

    void dgetrf_(int *, int *, double *, int *, int *, int *);

    void read_string(FILE *, char *, int );

    void output(FILE *, int, int, int, double *, double *, int, int);

    void read_number(int *, int *, FILE *);

    void read_data(double *, int *, int, int, FILE *);
