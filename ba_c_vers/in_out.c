#define STR_MAX 256
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include "fortran_interface.h"

void read_string(   FILE *file_id, 
                    char *str, 
                    int max_length)
{
    int len;
    fgets(str, max_length, file_id);
    len = strlen(str);
    while (len && isspace(str[len - 1]))
    {
        len--;
    }
    if (!len) 
    {
        printf("Wrong format input_ba\n");
        exit(0);
    }
    str[len] = 0;
    return;
}

void output(FILE *file_id, 
            int track_num, 
            int step_num, 
            int breakaway_num,
            double *track,
            double *points, 
            int point_num, 
            int obj_num)
{
    int i, j, k, n;
    float aux[3];

    if (step_num == 1) 
    {
        fwrite(&obj_num, 2, 1, file_id);
        fwrite(&point_num, 2, 1, file_id);
        fwrite(&breakaway_num, 2, 1, file_id);
    }

    fwrite(&step_num, 2, 1, file_id);
    for (i = 0; i < point_num; i++) 
    {
        aux[0] = points[2 * i];
        aux[1] = points[2 * i + 1];
        aux[2] = 0;
        fwrite(aux, sizeof(*aux), 3, file_id);
    }
    for (i = 0; i < breakaway_num; i++)
    {  
        n = i + 1;
        fwrite(&n, 2, 1, file_id);
        n= step_num + 1;
        fwrite(&n, 2, 1, file_id);
        for (k = 0; k < n; k++) 
        {
             j = k * breakaway_num + i;
             aux[0] = track[2 * j];
             aux[1] = track[2 * j + 1];
             aux[2] = 0;
             fwrite(aux, sizeof(*aux), 3, file_id);
        }
    }
    return;
}

void read_number(int *point_num,
                int *breakaway_num,
                FILE *data_id)
{
    int i;
    char str[STR_MAX];
    double aux_d;
    int aux_int;

    for (i = 0; i < 8; i++) 
    {
        fgets(str, STR_MAX, data_id);
    }
    i = -1;
    do {
        i++;
        fgets(str, STR_MAX, data_id);
    } while (sscanf(str, "%lf", &aux_d) == 1);
    *point_num = i;
    for (i = 0; i < 4; i++)
    {
        fgets(str, STR_MAX, data_id);
    }
    while (fscanf(data_id,"%d", &i));
    fgets(str, STR_MAX, data_id);
    fgets(str, STR_MAX, data_id);
    i = -1;
    do {
        i++;
    } while(fscanf(data_id, "%d", &aux_int) == 1);
    *breakaway_num = i;
//    printf("%d\n", *breakaway_num);
    return;
}

void read_data(double *points,
                int *breakaways,
                int point_num,
                int breakaway_num,
                FILE *data_id)
{
    int i;
    char str[STR_MAX];

    fseek(data_id, 0, SEEK_SET);
    for (i = 0; i < 8; i++) 
    {
        fgets(str, STR_MAX, data_id);
    }
    
    for (i = 0; i < point_num; i++)
    {
        fgets(str, STR_MAX, data_id);
        sscanf(str, "%lf%lf", &points[2 * i], &points[2 * i + 1]);
    }
//    printf("%lf\n", points[2 * i - 1]);
    for (i = 0; i < 5; i++)
    {
        fgets(str, STR_MAX, data_id);
    }
    while (fscanf(data_id,"%d", &i));
    fgets(str, STR_MAX, data_id);
    fgets(str, STR_MAX, data_id);
    for (i = 0; i < breakaway_num; i++)
    {
        fscanf(data_id, "%d", &breakaways[i]);    
    }
//    printf("%d %d\n", breakaways[0], breakaways[1]);
    return;
}
