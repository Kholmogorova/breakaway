#define STR_MAX 256
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "fortran_interface.h"

int main(void) {
    int
        point_num,
        step_num,
        breakaway_num, 
        cur_track_num, 
        track_num, 
        i, info;
    FILE   
        *input_id, 
        *data_id, 
        *parameter_id, 
        *sheet_id; 

    int
        *pivot_indicies,
        *breakaways;
    char
        data_file[PATH_MAX + 1],
        parameter_file[PATH_MAX + 1],
        sheet_file[PATH_MAX + 1];
    double
        aux,
        regular_speed[2],
        rotation_radius,
        rotation_radius_sq,
        time_interval;
    double 
        *rotation_points,
        *control_points,
        *normals,
        *gyration, 
        *system,
        *track,
        *track_speed,
        *track_circle_speed;
    clock_t time;

    regular_speed[0] = 1.0;
    regular_speed[1] = 0.0;

    input_id = fopen ("input_ba", "r");
    read_string(input_id, data_file, PATH_MAX + 1);
    read_string(input_id, parameter_file, PATH_MAX + 1);
    read_string(input_id, sheet_file, PATH_MAX + 1);
    fclose(input_id);

    data_id = fopen(data_file, "r");
    read_number(&point_num, &breakaway_num, data_id);

    parameter_id = fopen(parameter_file, "r");
    fscanf(parameter_id, "%lf\n%lf\n%d", &rotation_radius, 
            &time_interval, &step_num);
    fclose(parameter_id);

    track_num = breakaway_num * (step_num + 1);
    
    rotation_points = malloc(2 * point_num * sizeof(*rotation_points)); 
    control_points = malloc(2 * point_num * sizeof(*control_points)); 
    normals = malloc(2 * point_num * sizeof(*normals)); 
    gyration = malloc((point_num + 1) * sizeof(*gyration)); 
    system = malloc((point_num + 1) * (point_num + 1) * sizeof(*system)); 
    pivot_indicies = malloc((point_num + 1) * sizeof(*pivot_indicies)); 
    breakaways = malloc(breakaway_num * sizeof(*breakaways)); 
    track  = malloc(2 * track_num * sizeof(*track)); 
    track_speed = malloc(2 * track_num * sizeof(*track)); 
    track_circle_speed  = malloc(track_num * 
                                sizeof(*track_circle_speed)); 
    read_data(rotation_points, breakaways, point_num, 
                breakaway_num, data_id);
    fclose(data_id);

    rotation_radius_sq = rotation_radius * rotation_radius;
    
    get_control_points_(control_points,                          
            rotation_points, &point_num);
    get_normals_(normals, rotation_points, &point_num);
    
    aux = 0;
    get_system_(system, control_points,                          
            rotation_points, normals, &point_num, &aux);
    i = point_num + 1;
    dgetrf_(&i, &i, system, &i, pivot_indicies, &info);
    if (info != 0) 
    {
        printf("error in dgetrf\n");
        exit(1);
    }
    i = 0;
    solution_(&point_num, &i, gyration, normals, regular_speed,   
            control_points, track, track_circle_speed,              
            pivot_indicies, system, &rotation_radius_sq);

    for (i = 0; i < breakaway_num; i++)
    {
        track[2 * i] = rotation_points[breakaways[i] * 2 - 2];
        track[2 * i + 1] = rotation_points[breakaways[i] * 2 - 1];
    }
    cur_track_num = breakaway_num;

    sheet_id = fopen(sheet_file, "wb");

// basic cycle
    for (i = 1; i <= step_num; i++) 
    {
        time = clock();
        track_shift_(&point_num, &track_num, track, rotation_points,  
               track_circle_speed, gyration, regular_speed, 
               track_speed, &breakaway_num, &i, breakaways, 
               &time_interval, &rotation_radius, &rotation_radius_sq);
        time = clock() - time;
        printf("%d  %lf\n", cur_track_num + point_num - breakaway_num,
                (double)time/CLOCKS_PER_SEC);
        solution_(&point_num, &cur_track_num, gyration, normals, 
               regular_speed, control_points, track,
               track_circle_speed, pivot_indicies, system,
               &rotation_radius_sq);
        cur_track_num = cur_track_num + breakaway_num;
        output(sheet_id, cur_track_num, i, breakaway_num, track,
                rotation_points, point_num, 1);
    }
    
    fclose(sheet_id);

    free(rotation_points);
    free(control_points);
    free(normals);
    free(gyration);
    free(system);
    free(pivot_indicies);
    free(breakaways);
    free(track);
    free(track_speed);
    free(track_circle_speed);
    return 0;
}
