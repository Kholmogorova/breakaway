interface
    subroutine read_points(points, point_num, f_id)
        integer :: point_num, f_id
        real(8) :: points(2, point_num)
    end subroutine

    subroutine get_control_points(control_points, points, point_num)
        integer :: point_num
        real(8) :: control_points(2, point_num), points(2, point_num)
    end subroutine

    subroutine get_normals(normals, points, point_num)
        integer :: point_num
        real(8) :: normals(2, point_num), points(2, point_num)
    end subroutine

    subroutine print_results(results, dimen, point_num, f_id)
        integer :: point_num, f_id, dimen
        real(8) :: results(dimen, point_num)
    end subroutine

    subroutine get_right_part(right_part, normals, regular_speed, &
            control_points, track, track_circle_speed, &
            track_num, point_num, rotation_radius_sq)
        integer :: point_num, track_num
        real(8) :: right_part(point_num + 1)
        real(8) :: normals(2, point_num), regular_speed(2)
        real(8) :: control_points(2, point_num), track(2, track_num)
        real(8) :: track_circle_speed(track_num), rotation_radius_sq
    end subroutine

    subroutine get_system(system, control_points, points, &
                        normals, point_num, rotation_radius_sq)
        integer :: point_num, i, j
        real(8) ::  control_points(2, point_num), &
                    points(2, point_num), &
                    normals(2, point_num), &
                    system(point_num + 1, point_num + 1),   &
                    step_size, rotation_radius_sq
    end subroutine

    subroutine print_table(right_part, point_num, res_id)
        integer :: point_num, res_id
        real(8) :: right_part(point_num + 1)
    end subroutine

    subroutine full_speed(points, circle_speed, point_num, &
                        place, res, rotation_radius_sq)
        integer :: point_num
        real(8) :: points(2, point_num), circle_speed(point_num) 
        real(8) :: res(2), place(2), rotation_radius_sq
    end subroutine

    subroutine check_ortho(control_points, normals, point_num, points,&
                        circle_speed, regular_speed, file_id)
    integer :: point_num, file_id
    real(8) :: control_points(2, point_num), normals(2, point_num), &
                circle_speed(point_num), regular_speed(2), &
                points(2, point_num)
    end subroutine

    subroutine read_net(point_id, net_size, net_point)
        integer :: point_id, net_size
        real(8) :: net_point(2, net_size)
    end subroutine

    subroutine create_speed(points, circle_speed, point_num, &
                net_size, net_point, regular_speed, speed,          &
                rotation_radius_sq)
        integer :: point_num, net_size
        real(8) :: points(2, point_num), circle_speed(point_num), &
                net_point(2, net_size), regular_speed(2), &
                speed(2, net_size), rotation_radius_sq
    end subroutine

    subroutine write_speed(speed_id, net_size, speed)
        integer :: net_size, speed_id
        real(8) :: speed(2, net_size)
    end subroutine

    subroutine speed_with_track(target_point, res, &
            track_num, point_num, track, points, &
            track_circle_speed, circle_speed, regular_speed, &
            rotation_radius_sq)
        integer :: point_num, track_num
        real(8) :: target_point(2), res(2), track(2, track_num)
        real(8) :: points(2, point_num), circle_speed(point_num)
        real(8) :: track_circle_speed(track_num), regular_speed(2)
        real(8) :: rotation_radiudius_sq
    end subroutine

    subroutine track_shift(point_num, track_num, track, points, &
            track_circle_speed, circle_speed, regular_speed, &
            track_speed, breakaway_num, step_num, breakaways, &
            time_interval, rotation_radius, rotation_radius_sq)
        integer :: point_num, track_num, breakaway_num, step_num
        real(8) :: track(2, track_num), points(2, point_num)
        real(8) :: track_circle_speed(track_num), circle_speed(point_num)
        real(8) :: regular_speed(2), track_speed(2, track_num)
        integer :: breakaways(breakaway_num)
        real(8) :: time_interval, rotation_radius, rotation_radius_sq
    end subroutine

    subroutine read_parameters(file_id, radius, time,                   &
                step_number, breakaway_num)
        integer :: file_id, step_number, breakaway_num
        real(8) :: radius, time
    end subroutine

    subroutine output_geometry(file_id, body_num, module_num, closed,   &
                        point_num, points, breakaway_num, breakaways)
        integer ::                                                      &
                file_id,                                                &
                body_num,                                               &
                module_num,                                             &
                closed,                                                 &
                point_num,                                              &
                breakaway_num,                                          &
                breakaways(breakaway_num)
        real(8) :: points(point_num)
    end subroutine

    subroutine solution(point_num, track_num, gyration, normals,        &
                regular_speed, control_points, track,                   &
                track_circle_speed, pivot_indicies, system,             &
                rotation_radius_sq)
        integer :: point_num, track_num, pivot_indicies(point_num + 1)
        real(8) ::  gyration(point_num + 1),                            &
                normals(point_num),                                 &
                regular_speed(2),                                   &
                control_points(2, point_num),                       &
                track(2, track_num),                                &
                track_circle_speed(track_num),                      &
                system(point_num + 1, point_num + 1),               &
                rotation_radius_sq
    end subroutine

    subroutine make_sheet(file_id, track_num, step_num,                 &
                breakaway_num, track)
        integer :: file_id, track_num, breakaway_num, step_num
        real(8) :: track(2, track_num)
    end subroutine
end interface
