subroutine get_step_size(points)
use step
implicit none
    real(8) :: points(2, 2), aux(2)

    aux = points(:, 2) - points(:, 1)
    step_size = sqrt(dot_product(aux, aux))
end subroutine
    
subroutine read_points(points, point_num, f_id)
implicit none
    integer :: point_num, f_id, i, j
    real(8) :: points(2, point_num)

    do i = 1, point_num
        read (f_id, *) (points(j, i), j = 1, 2)
    end do
end subroutine

subroutine get_control_points(control_points, points, point_num)
implicit none
    integer :: point_num, i
    real(8) :: control_points(2, point_num), points(2, point_num)

    do i = 1, point_num - 1
        control_points(:, i) = (points(:, i) + points(:, i + 1)) / 2
    end do
    control_points(:, point_num) = (points(:, point_num) + &
                                    points(:, 1)) / 2
end 

subroutine get_normals(normals, points, point_num)
implicit none
    integer :: point_num, i
    real(8) :: normals(2, point_num), points(2, point_num)
    real(8) :: aux_vect(2), norm

    do i = 1, point_num - 1
        aux_vect = points(:, i + 1) - points(:, i)
        norm = sqrt(dot_product(aux_vect, aux_vect))
        normals(1, i) = aux_vect(2) / norm
        normals(2, i) = -aux_vect(1) / norm
    end do
    aux_vect = points(:, 1) - points(:, point_num)
    norm = sqrt(dot_product(aux_vect, aux_vect))
    normals(1, point_num) = aux_vect(2) / norm
    normals(2, point_num) = -aux_vect(1) / norm
end subroutine

subroutine print_results(results, dimen, point_num, f_id)
implicit none
    integer :: point_num, f_id, dimen, i, j
    real(8) :: results(dimen, point_num)

    do i = 1, point_num
        write(f_id, *) (results(j, i), j = 1, dimen)
    end do
end subroutine

subroutine print_table(right_part, point_num, res_id)
implicit none
    integer :: point_num, res_id, i
    real(8) :: right_part(point_num + 1), step
    real(8) :: pi = 3.141592653589793
    
    do i = 1, point_num
        step = pi * 2 / point_num
        write(res_id, *) i, step * i, right_part(i), right_part(i) / step
    end do
end subroutine

subroutine check_ortho(control_points, normals, point_num, points,&
                        circle_speed, regular_speed, file_id)
implicit none
    integer :: point_num, i, file_id
    real(8) :: control_points(2, point_num), normals(2, point_num), &
                circle_speed(point_num), regular_speed(2), aux(2), &
                points(2, point_num)

    do i = 1, point_num
        call full_speed(points, circle_speed, point_num, &
                    control_points(:, i), aux)
        aux = aux + regular_speed
        write(file_id, *) dot_product(aux, normals(:, i))
    end do
end subroutine

subroutine read_net(point_id, net_size, net_point)
implicit none
    integer :: point_id, net_size
    real(8) :: net_point(2, net_size)
    integer :: i, j

    do i = 1, net_size
        read(point_id, *) (net_point(j, i), j = 1, 2)
    end do
end subroutine

subroutine write_speed(speed_id, net_size, speed)
    integer :: net_size, speed_id
    real(8) :: speed(2, net_size)
    integer :: i, j

    do i = 1, net_size
        write(speed_id, *) (speed(j, i), j = 1, 2), 0.d0
    end do
end subroutine

subroutine read_parameters(file_id, radius, time,                       & 
            step_number, breakaway_num)
    integer :: file_id, step_number, breakaway_num
    real(8) :: radius, time
        read(file_id, *) radius
        read(file_id, *) time
        read(file_id, *) step_number
        read(file_id, *) breakaway_num
end subroutine

subroutine output_geometry(file_id, body_num, module_num, closed,       &
                        point_num, points, breakaway_num, breakaways)
    integer ::                                                          &
                file_id,                                                &
                body_num,                                               &
                module_num,                                             &
                closed,                                                 &
                point_num,                                              &
                breakaway_num,                                          &
                breakaways(breakaway_num)
    real(8) :: points(2, point_num)
    integer :: i
    
    write(file_id, *) body_num
    write(file_id, *) module_num
    write(file_id, *) closed
    write(file_id, *) point_num
    write(file_id, *) 1, 32

    do i = 1, point_num - 1
        write(file_id, 1000)   points(1, i), points(2, i), 0.d0,           &
                            points(1, i), points(2, i), -0.05,         &
                            points(1, i + 1), points(2, i + 1), -0.05, &
                            points(1, i + 1), points(2, i + 1), 0.d0    
    end do
    write(file_id, 1000)                                               &   
                    points(1, point_num), points(2, point_num), 0.d0,   &
                    points(1, point_num), points(2, point_num), -0.05, &
                    points(1, 1), points(2, 1), -0.05,                 &
                    points(1, 1), points(2, 1), 0.d0                
    write(file_id, *) breakaway_num
    do i = 1, breakaway_num
        write(file_id, *) 1
        write(file_id, 1000)                                           &
        points(1, breakaways(i)), points(2, breakaways(i)), 0d0,       &
        points(1, breakaways(i)), points(2, breakaways(i)), -0.05      
    end do
1000  format(12f8.4)

end subroutine

subroutine make_sheet(file_id, track_num, step_num, breakaway_num, track)
implicit none
    integer :: file_id, track_num, breakaway_num, step_num
    real(8) :: track(2, track_num)

    integer :: i, j, k

    write(file_id) step_num + 1
    write(file_id) breakaway_num
    do i = 1, breakaway_num
            write(file_id) 2, step_num + 1
        do k = 1, step_num + 1
            j = (k - 1) * breakaway_num + i
            write(file_id)  sngl(track(1, j)), sngl(track(2, j)), 0.0
            write(file_id)  sngl(track(1, j)), sngl(track(2, j)), -0.05
        end do
    write(file_id) 0
    write(file_id) 0
    end do
end subroutine

subroutine output(file_id, track_num, step_num, breakaway_num, track,   &
                    points, point_num, obj_num)
implicit none
    integer :: file_id, track_num, breakaway_num, step_num, point_num
    real(8) :: track(2, track_num), points(2, point_num)

    integer*2 :: i, j, k, obj_num

    if (step_num .eq. 1) then
        write(file_id) int2(obj_num)
        write(file_id) int2(breakaway_num)
    end if

    write(file_id) int2(step_num)
    do i = 1, point_num
        write(file_id) sngl(points(1, i)), sngl(points(2, i)), 0.0
    end do

    do i = 1, breakaway_num
            write(file_id) i
            write(file_id) step_num + 1
        do k = 1, step_num + 1
            j = (k - 1) * breakaway_num + i
            write(file_id)  sngl(track(1, j)), sngl(track(2, j)), 0.0
        end do
    end do
end subroutine
