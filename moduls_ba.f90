module constants
    real, parameter :: pi = 3.14159265359
end module constants

module step
    real(8) :: step_size = 0
    real(8) :: rotation_radius_sq = 0
    real(8) :: rotation_radius = 0
end module

